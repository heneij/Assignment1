﻿namespace Assignment1
{
    /// <summary>
    /// Class to contain secondary attributes: Health,ArmorRating,and ElementalResistance
    /// </summary>
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance{ get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public SecondaryAttributes()
        {
            
        }

        /// <summary>
        /// method that sets the attributes based on input
        /// </summary>
        /// <param name="atts">int[]array with values to assign to the attributes</param>
        public void AdjustAttributes(int[] atts)
        {
            Health = atts[0];
            ArmorRating = atts[1];
            ElementalResistance = atts[2];
        }

    }
}