﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class Mage : Hero
    {
        /// <summary>
        /// Constructor for Mage, constructs an EquipmentType-array to keep track of legal equipmenttypes for Mages
        /// </summary>
        /// <param name="name">The desired name for the Mage</param>
        public Mage(string name) : base(name) 
        {
            PossibleEquipment = new EquipmentType[] { EquipmentType.WEAPON_STAFF, EquipmentType.WEAPON_WAND, EquipmentType.ARMOR_CLOTH };
        }
        
        public override PrimaryAttributes CalculateBaseAttributes()
        {
            return new PrimaryAttributes(2 + Level * 3, Level, Level, 3 + Level * 5);
        }

        public override void UpdateTotalDamageBasedOnAttributes()
        {
            TotalDamagePerSecond = BaseDamagePerSecond * (1 + TotalAtts.Intelligence / 100);
        }
    }
}
