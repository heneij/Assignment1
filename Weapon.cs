﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class Weapon : Equipment
    {
        public int BaseDamage { get; set; }
        public double AttackSpeed { get; set; }

        /// <summary>
        /// constructor for Weapon, assigns base damage, attackspeed, and slot type
        /// </summary>
        /// <param name="name">desired name for this equipment</param>
        /// <param name="reqLevel">required level to equip this equipment</param>
        /// <param name="eqType">the type of equipment</param>
        /// <param name="dam">the damage for this weapon</param>
        /// <param name="atSpeed">the attack speed for this weapon</param>
        public Weapon(string name, int reqLevel, EquipmentType eqType, int dam, double atSpeed) : base(name, reqLevel, eqType)
        {
            this.BaseDamage = dam;
            this.AttackSpeed = atSpeed;
            this.Slot = SlotType.SLOT_WEAPON;
        }

        public override void AddEquipmentBonus(Hero hero)
        {
            hero.BaseDamagePerSecond = BaseDamage * AttackSpeed;
        }
    }
}
