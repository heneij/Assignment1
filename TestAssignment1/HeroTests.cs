using Assignment1;
using System;
using Xunit;

namespace TestAssignment1
{
    public class HeroTests
    {
        [Fact]
        public void HeroConstructor_ShouldBeLevelOne()
        {
            //Arrange
            Mage mage = new Mage("Steve");
            //Assert
            Assert.Equal(1, mage.Level);
        }

        [Fact]
        public void LevelUp_ValidLevels_ShouldBeLevelTwo()
        {
            //Arrange
            Mage mage = new Mage("Steve");
            //Act
            mage.LevelUp(1);
            //Assert
            Assert.Equal(2, mage.Level);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_ZeroOrNegativeLevels_ShouldThrowArgumentException(int inn)
        {
            //Arrange
            Mage mage = new Mage("Steve");
            //Act & Assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(inn));
        }

        [Fact]
        public void GetAttributeArray_Mage_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Mage mage = new Mage("Peter");
            //Assert
            Assert.Equal(new int[] {5, 1, 1, 8, 5, 1, 1, 8, 80, 6, 1}, mage.GetAttributeArray());
        }

        [Fact]
        public void GetAttributeArray_Ranger_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger("Homer");
            //Assert
            Assert.Equal(new int[] { 8, 1, 7, 1, 8, 1, 7, 1, 10, 9, 7 }, ranger.GetAttributeArray());
        }

        [Fact]
        public void GetAttributeArray_Rogue_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue("Marge");
            //Assert
            Assert.Equal(new int[] { 8, 2, 6, 1, 8, 2, 6, 1, 10, 10, 6 }, rogue.GetAttributeArray());
        }

        [Fact]
        public void GetAttributeArray_Warrior_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior("John");
            //Assert
            Assert.Equal(new int[] { 10, 5, 2, 1, 10, 5, 2, 1, 10, 15, 2 }, warrior.GetAttributeArray());
        }
        //BREAAAAKA
        [Fact]
        public void GetAttributeArray_LeveledUpMage_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Mage mage = new Mage("Peter");
            //Act
            mage.LevelUp(1);
            //Assert
            Assert.Equal(new int[] { 8, 2, 2, 13, 8, 2, 2, 13, 130, 10, 2 }, mage.GetAttributeArray());
        }

        [Fact]
        public void GetAttributeArray_LeveledUpRanger_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Ranger ranger = new Ranger("Homer");
            //Act
            ranger.LevelUp(1);
            //Assert
            Assert.Equal(new int[] { 10, 2, 12, 2, 10, 2, 12, 2, 20, 12, 12 }, ranger.GetAttributeArray());
        }

        [Fact]
        public void GetAttributeArray_LeveledUpRogue_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Rogue rogue = new Rogue("Marge");
            //Act
            rogue.LevelUp(1);
            //Assert
            Assert.Equal(new int[] { 11, 3, 10, 2, 11, 3, 10, 2, 20, 14, 10 }, rogue.GetAttributeArray());
        }

        [Fact]
        public void GetAttributeArray_LeveledUpWarrior_ShouldHaveCorrectAttributes()
        {
            //Arrange
            Warrior warrior = new Warrior("John");
            //Act
            warrior.LevelUp(1);
            //Assert
            Assert.Equal(new int[] { 15, 8, 4, 2, 15, 8, 4, 2, 20, 23, 4 }, warrior.GetAttributeArray());
        }
    }
}
