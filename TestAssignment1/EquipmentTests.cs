﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment1;
using Xunit;

namespace TestAssignment1
{
    public class EquipmentTests
    {
        [Fact]
        public void Equip_InvalidWeaponLevel_ShouldThrow()
        {
            //Arrange
            Warrior warrior = new("Nean");
            Weapon testAxe = new Weapon("Common Axe", 2, EquipmentType.WEAPON_AXE, 7, 1.1);
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
        }

        [Fact]
        public void Equip_InvalidArmorLevel_ShouldThrow()
        {
            //Arrange
            Warrior warrior = new("Nean");
            Armor testPlateBody = new Armor("Common Plate Body Armor", 2, EquipmentType.ARMOR_PLATE, SlotType.SLOT_BODY, new PrimaryAttributes(1, 0, 0, 2));
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateBody));
        }

        [Fact]
        public void Equip_InvalidWeaponType_ShouldThrow()
        {
            //Arrange
            Warrior warrior = new("Nean");
            Weapon testBow = new Weapon("Common Bow", 1, EquipmentType.WEAPON_BOW, 12, 0.8);
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
        }

        [Fact]
        public void Equip_InvalidArmorType_ShouldThrow()
        {
            //Arrange
            Warrior warrior = new("Nean");
            Armor testClothHead = new Armor("Common Cloth Head Armor", 1, EquipmentType.ARMOR_CLOTH, SlotType.SLOT_HEAD, new PrimaryAttributes(0, 0, 5, 1));
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothHead));
        }

        [Fact]
        public void Equip_ValidWeapon_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior warrior = new("Nean");
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentType.WEAPON_AXE, 7, 1.1);
            //Act
            string returnMessage = warrior.EquipItem(testAxe);
            //Assert
            Assert.Equal("Nean has successfully equiped a Common Axe", returnMessage);
        }

        [Fact]
       public void Equip_ValidArmor_ShouldReturnSuccessMessage()
        {
            //Arrange
            Warrior warrior = new("Nean");
            Armor testPlateBody = new Armor("Common Plate Body Armor", 1, EquipmentType.ARMOR_PLATE, SlotType.SLOT_BODY, new PrimaryAttributes(1, 0, 0, 2));
            //Act
            string returnMessage = warrior.EquipItem(testPlateBody);
            //Assert
            Assert.Equal("Nean has successfully equiped a Common Plate Body Armor", returnMessage);
        }

        [Fact]
        public void Damage_LevelOneNoEquipment_ShouldHaveCorrectDamage()
        {
            //Arrange
            Warrior warrior = new("Nean");
            //Act
            double expectedDamage = warrior.TotalDamagePerSecond;
            //Assert
            Assert.Equal(1 * (1 + (5 / 100)),expectedDamage);
        }

        [Fact]
        public void Damage_LevelOneWithCommonAxe_ShouldHaveCorrectDamage()
        {
            //Arrange
            Warrior warrior = new("Nean");
            //Act
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentType.WEAPON_AXE, 7, 1.1);
            warrior.EquipItem(testAxe);
            double expectedDamage = warrior.TotalDamagePerSecond;
            //Assert
            Assert.Equal((7 * 1.1) * (1 + (5 / 100)), expectedDamage);
        }

        [Fact]
        public void Damage_LevelOneWithAxeAndArmor_ShouldHaveCorrectDamage()
        {
            //Arrange
            Warrior warrior = new("Nean");
            //Act
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentType.WEAPON_AXE, 7, 1.1);
            Armor testPlateBody = new Armor("Common Plate Body Armor", 1, EquipmentType.ARMOR_PLATE, SlotType.SLOT_BODY, new PrimaryAttributes(1, 0, 0, 2));
            warrior.EquipItem(testAxe); 
            warrior.EquipItem(testPlateBody);
            
            double expectedDamage = warrior.TotalDamagePerSecond;
            //Assert
            Assert.Equal((7 * 1.1) * (1 + ((5+1) / 100)), expectedDamage);
        }




        private Tuple<Weapon, Weapon, Armor, Armor> MakeTestItems()
        {
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentType.WEAPON_AXE, 7, 1.1);
            Weapon testBow = new Weapon("Common Bow", 1, EquipmentType.WEAPON_BOW, 12, 0.8);
            Armor testPlateBody = new Armor("Common Plate Body Armor", 1, EquipmentType.ARMOR_PLATE, SlotType.SLOT_BODY, new PrimaryAttributes(1, 0, 0, 2));
            Armor testClothHead = new Armor("Common Cloth Head Armor", 1, EquipmentType.ARMOR_CLOTH, SlotType.SLOT_HEAD, new PrimaryAttributes(0, 0, 5, 1));
            return Tuple.Create(testAxe, testBow, testPlateBody, testClothHead);
        }
    }
}
