﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class Warrior : Hero
    {
        /// <summary>
        /// Constructor for Warrior, constructs an EquipmentType-array to keep track of legal equipmenttypes for Warriors
        /// </summary>
        /// <param name="name">The desired name for the Warrior</param>
        public Warrior(string name) : base(name)
        {
            PossibleEquipment = new EquipmentType[] {EquipmentType.ARMOR_MAIL, EquipmentType.ARMOR_PLATE, EquipmentType.WEAPON_AXE, EquipmentType.WEAPON_HAMMER, EquipmentType.WEAPON_SWORD};
        }

        public override PrimaryAttributes CalculateBaseAttributes()
        {
            return new PrimaryAttributes(5 + Level * 5, 2 + Level * 3, Level * 2, Level );
        }

        public override void UpdateTotalDamageBasedOnAttributes()
        {
            TotalDamagePerSecond = BaseDamagePerSecond * (1 + TotalAtts.Strength / 100);
        }
    }
}
