﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class Armor : Equipment
    {
        public PrimaryAttributes EquipmentAttributes { get; set; }

        /// <summary>
        /// Constructor for armor, assigns the slot type and primary attributes
        /// </summary>
        /// <param name="name">desired name for this equipment</param>
        /// <param name="reqLevel">required level to equip this equipment</param>
        /// <param name="eqType">the type of equipment</param>
        /// <param name="slType">the type of slot used when equiping this equipment</param>
        /// <param name="atts">the primary attributes that a hero gains when wearing this equipment</param>
        public Armor(string name, int reqLevel, EquipmentType eqType, SlotType slType, PrimaryAttributes atts) : base(name, reqLevel, eqType)
        {
            Slot = slType;
            this.EquipmentAttributes = atts;
        }

        public override void AddEquipmentBonus(Hero hero)
        {
            hero.TotalAtts += EquipmentAttributes;
        }
    }
}
