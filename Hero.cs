﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//namespace
namespace Assignment1
{
    /// <summary>
    /// The parent class for all heros.
    /// </summary>
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }

        public double BaseDamagePerSecond { get; set; }
        public double TotalDamagePerSecond { get; set; }
        public PrimaryAttributes TotalAtts { get; set; }
        public PrimaryAttributes BaseAtts { get; set; }
        public SecondaryAttributes SecondaryAtts { get; set; }

        public Dictionary<SlotType, Equipment> EquipmentSlots { get; set; }
        public EquipmentType[] PossibleEquipment { get; set; }

        /// <summary>
        /// Base constructor for the abstract Hero class. Assigns a name, and initializes all attributes.
        /// </summary>
        /// <param name="name">The desired name for the Hero</param>
        public Hero(string name)
        {
            this.Name = name;
            this.Level = 1;
            this.BaseDamagePerSecond = 1.0;
            this.TotalDamagePerSecond = 1.0;
            BaseAtts = new PrimaryAttributes();
            TotalAtts = new PrimaryAttributes();
            SecondaryAtts = new SecondaryAttributes();
            EquipmentSlots = new();
            AdjustBaseAttributes();
            AdjustBonusAttributes();
            AdjustSecondaryAttributes();

        }

        /// <summary>
        /// Equips the specified item to this Hero. Throws InvalidArmorExc
        /// </summary>
        /// <param name="item">item of type Equipment that is supposed to be equiped.</param>
        /// <returns>A message confirming the item has been equiped.</returns>
        /// <exception>Throws InvalidArmorException or InvalidWeaponException if the Hero is not able to equip the item.</exception>
        public string EquipItem(Equipment item)
        {

            if (CheckIfEquipable(item.RequiredLevel, item.EqType))
            {
                EquipmentSlots.Add(item.Slot, item);

                item.AddEquipmentBonus(this);

                UpdateTotalDamageBasedOnAttributes();
                
                AdjustSecondaryAttributes();
                return(Name + " has successfully equiped a " + item.Name);
            }
            else
            {
                if (item.Slot == SlotType.SLOT_WEAPON) //Would it make more sense to have an InvalidEquipmentException?
                {
                    throw new InvalidWeaponException();
                }
                else
                {
                    throw new InvalidArmorException();
                }
            }
        }

        /// <summary>
        /// Check if this Hero can equip a weapon given a required level and equipment type
        /// </summary>
        /// <param name="requiredLevel">the reaquired level that is to be checked</param>
        /// <param name="eqType">the type of equipment to check compatability with</param>
        /// <returns>returns true if this Hero can equip a weapon with the specified attributes</returns>
        public bool CheckIfEquipable(int requiredLevel, EquipmentType eqType)
        {
            if ((this.Level >= requiredLevel) & PossibleEquipment.Contains(eqType))
            {
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Adjusts the Base Primary Attributes
        /// </summary>
        public void AdjustBaseAttributes()
        {
            BaseAtts = CalculateBaseAttributes();
        }

        /// <summary>
        /// Calculates the base primary attributes based on the level and type of Hero
        /// </summary>
        /// <returns>returns a PrimaryAttribute object containing the calculated base primary attributes</returns>
        public abstract PrimaryAttributes CalculateBaseAttributes();

        /// <summary>
        /// Loops through all equipment that the Hero has equiped and updates the total primary attributes
        /// </summary>
        public void AdjustBonusAttributes()
        {
            TotalAtts = BaseAtts;
            foreach (KeyValuePair<SlotType, Equipment> entry in EquipmentSlots ?? new Dictionary<SlotType, Equipment>())
            {
                entry.Value.AddEquipmentBonus(this);
            }
        }

        /// <summary>
        /// Updates the total total damage per second based on the base damage per second and the total primary attributes
        /// </summary>
        public abstract void UpdateTotalDamageBasedOnAttributes();

        /// <summary>
        /// Adjusts the secondary attributes based on the total primary attributes
        /// </summary>
        public void AdjustSecondaryAttributes()
        {
            SecondaryAtts.AdjustAttributes(CalculateSecondaryAttributes());
        }

        /// <summary>
        /// Calculates the secondary attributes based on the total primary attributes
        /// </summary>
        /// <returns>returns an int[] containing the calculated secondary attributes</returns>
        public int[] CalculateSecondaryAttributes()
        {
            return new int[] { 10 * TotalAtts.Vitality, TotalAtts.Strength + TotalAtts.Dexterity, TotalAtts.Intelligence };
        }

        /// <summary>
        /// Increases the Heros level, and adjusts its attributes accordingly
        /// </summary>
        /// <param name="lvls">the amount of levels to be added</param>
        /// <exception>Throws ArgumentException if trying to gain zero or a negative amount of levels</exception>
        public void LevelUp(int lvls)
        {
            if (lvls < 1)
            {
                throw new ArgumentException("It is impossible to gain zero or a negative amount of levels");
            }
            else
            {
                Level += lvls;
                AdjustBaseAttributes();
                AdjustBonusAttributes();
                UpdateTotalDamageBasedOnAttributes();
                AdjustSecondaryAttributes();
            }
        }

        /// <summary>
        /// For testing purposes only. accesses the Primary and Secondary attributes of this Hero
        /// </summary>
        /// <returns>and int[] containing baseAtts,totalAtts,SecondaryAtts</returns>
        public int[] GetAttributeArray()
        {
            return new int[] { BaseAtts.Strength, BaseAtts.Dexterity, BaseAtts.Intelligence, BaseAtts.Vitality, TotalAtts.Strength, TotalAtts.Dexterity, TotalAtts.Intelligence, TotalAtts.Vitality, SecondaryAtts.Health, SecondaryAtts.ArmorRating, SecondaryAtts.ElementalResistance };
        }

        /// <summary>
        /// Arranges all hero stats into a string
        /// </summary>
        /// <returns>string containing all hero stats</returns>
        public string DisplayHeroStats()
        {
            string s = $"Character Name: {this.Name}\nCharacter Level: {this.Level}\n" +
                $"Strength: {this.TotalAtts.Strength}\nDexterity: {this.TotalAtts.Dexterity}\n" +
                $"Intelligence: {this.TotalAtts.Intelligence}\nVitality: {this.TotalAtts.Vitality}\n" +
                $"Health: {this.SecondaryAtts.Health}\nArmor Rating: {this.SecondaryAtts.ArmorRating}\n" +
                $"Elemental Resistance: {this.SecondaryAtts.ElementalResistance}\nDPS: {this.TotalDamagePerSecond}";
            return s;
        }
    }


    /// <summary>
    /// Exception to be thrown when a hero tries to equip a weapon which it is not able to, either because of level requirements, or compatability with equipment type
    /// </summary>
    [Serializable]
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException() { }

        public InvalidWeaponException(string message) : base(message) { }

        public InvalidWeaponException(string message, Exception inner) : base(message, inner) { }
    }

    /// <summary>
    /// Exception to be thrown when a hero tries to equip a piece of armor which it is not able to, either because of level requirements, or compatability with equipment type
    /// </summary>
    [Serializable]
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException() { }

        public InvalidArmorException(string message) : base(message) { }

        public InvalidArmorException(string message, Exception inner) : base(message, inner) { }
    }
}
