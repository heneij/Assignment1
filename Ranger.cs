﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class Ranger : Hero
    {
        /// <summary>
        /// Constructor for Ranger, constructs an EquipmentType-array to keep track of legal equipmenttypes for Rangers
        /// </summary>
        /// <param name="name">The desired name for the Ranger</param>
        public Ranger(string name) : base(name)
        {
            PossibleEquipment = new EquipmentType[] { EquipmentType.ARMOR_LEATHER, EquipmentType.ARMOR_MAIL, EquipmentType.WEAPON_BOW };
        }

        public override PrimaryAttributes CalculateBaseAttributes()
        {
            return new PrimaryAttributes(6 + Level * 2, Level, 2 + Level * 5, Level);
        }

        public override void UpdateTotalDamageBasedOnAttributes()
        {
            TotalDamagePerSecond = BaseDamagePerSecond * (1 + TotalAtts.Dexterity / 100);
        }

    }
}
