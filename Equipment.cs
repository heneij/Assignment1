﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    /// <summary>
    /// abstract class for all kinds of equipment
    /// </summary>
    public abstract class Equipment
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public SlotType Slot { get; set; }
        public EquipmentType EqType { get; set; }
        public abstract void AddEquipmentBonus(Hero hero);

        /// <summary>
        /// Constructor for Equipment, sets name, required level, and equipment type
        /// </summary>
        /// <param name="name">desired name for this equipment</param>
        /// <param name="reqLevel">required level to equip this equipment</param>
        /// <param name="eqType">the type of equipment</param>
        public Equipment(string name, int reqLevel, EquipmentType eqType)
        {
            this.Name = name;
            this.RequiredLevel = reqLevel;
            this.EqType = eqType;
        }

        
    }

    /// <summary>
    /// Enum to differ between slot types
    /// </summary>
    public enum SlotType
    {
        SLOT_WEAPON,
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEGS
    }

    /// <summary>
    /// enum to differ between equipment types
    /// </summary>
    public enum EquipmentType
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE,
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }
}
