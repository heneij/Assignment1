﻿namespace Assignment1
{
    /// <summary>
    /// Class to contain primary attributes: Strength, Dexterity, Intelligence, and Vitality
    /// </summary>
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }

        /// <summary>
        /// empty constructor
        /// </summary>
        public PrimaryAttributes()
        {

        }

        /// <summary>
        /// constructor that takes values for the 4 attributes and assigns them att creation
        /// </summary>
        /// <param name="s">Strength</param>
        /// <param name="d">Dexterity</param>
        /// <param name="i">Intelligence</param>
        /// <param name="v">Vitality</param>
        public PrimaryAttributes(int s=0, int d=0, int i=0, int v=0)
        {
            AdjustAttributes(s, d, i, v );
        }

        /// <summary>
        /// Method to set the attributes based on input
        /// </summary>
        /// <param name="s">Strength</param>
        /// <param name="d">Dexterity</param>
        /// <param name="i">Intelligence</param>
        /// <param name="v">Vitality</param>
        public void AdjustAttributes(int s, int d, int i, int v)
        {
            Strength = s;
            Dexterity = d;
            Intelligence = i;
            Vitality = v;
            
        }

        /// <summary>
        /// overloading the + operator. Adding two instances of PrimaryAttribute will return a new PrimaryAttributes with their combined values for their attributes.
        /// </summary>
        /// <param name="a">left side</param>
        /// <param name="b">right side</param>
        /// <returns>PrimaryAttributes with the combined attributes from the two inputed PrimaryAttributes</returns>
        public static PrimaryAttributes operator+ (PrimaryAttributes a, PrimaryAttributes b)
        {
            return new PrimaryAttributes(a.Strength + b.Strength, a.Dexterity + b.Dexterity, a.Intelligence + b.Intelligence, a.Vitality + b.Vitality);
        }
    }
}