﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public class Rogue : Hero
    {
        /// <summary>
        /// Constructor for Rogue, constructs an EquipmentType-array to keep track of legal equipmenttypes for Rogues
        /// </summary>
        /// <param name="name">The desired name for the Rogue</param>
        public Rogue(string name) : base(name)
        {
            PossibleEquipment = new EquipmentType[] { EquipmentType.WEAPON_DAGGER, EquipmentType.WEAPON_SWORD, EquipmentType.ARMOR_LEATHER, EquipmentType.ARMOR_MAIL };
        }

        public override PrimaryAttributes CalculateBaseAttributes()
        {
            return new PrimaryAttributes(5 + Level * 3, 1 + Level, 2 + Level * 4, Level);
        }

        public override void UpdateTotalDamageBasedOnAttributes()
        {
            TotalDamagePerSecond = BaseDamagePerSecond * (1 + TotalAtts.Dexterity / 100);
        }
    }
}
